let config = require("./common/config.js")
let utils = require("./common/utils.js")
// 导入常量类
let serviceConstant = require('./common/serviceConstant.js')
// 业务储存对象
var serviceStorage = storages.create("zjh336.cn" + config.serviceScriptKey);

let serviceUi = {}


// 初始业务uixml内容
serviceUi.initXml = () => {
  
    // 业务基本设置 
    ui.inflate(
        <card marginTop="40px" contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
            <vertical>
                <text text="基本业务设置:" textSize="22sp" textColor="#210303" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="旅行点:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <spinner id="select旅行点" entries="" h="*" w="*" gravity="left|center" layout_weight="2" popupBackground="#ffffff" />
                </horizontal>
                <horizontal h="80px">
                    <text text="悬赏点:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <spinner id="select悬赏" entries="" h="*" w="*" gravity="left|center" layout_weight="2" popupBackground="#ffffff" />
                </horizontal>
                <horizontal h="80px">
                    <text text="悬赏难度:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <radiogroup id="select难度" h="*" w="*" gravity="left|center" layout_weight="2" orientation="horizontal">
                    </radiogroup>
                </horizontal>
                <horizontal h="80px">
                    <text text="队伍模式:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <radiogroup id="队伍selectModel" h="*" w="*" gravity="left|center" layout_weight="2" orientation="horizontal">
                    </radiogroup>
                </horizontal>
                <horizontal h="80px" id="固定识图">
                    <text text="选择队伍:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <spinner id="select队伍" entries="" h="*" w="*" gravity="left|center" layout_weight="2" popupBackground="#ffffff" />
                </horizontal>
                <horizontal h="80px" id="自定义识字">
                    <text text="队伍名称:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <input id="填写队伍" hint="请填写队伍名称,默认按照110阈值匹配" textSize="16sp" inputType="text" h="*" w="*" margin="0" bg="#ffffff" padding="15px 0 0 0" gravity="left|center" layout_weight="2" />
                </horizontal>
            </vertical>
        </card>,
        ui.uiView,
        true
    )

    // 业务对战设置
    ui.inflate(
        <card marginTop="40px" contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
            <vertical>
                <text text="对战设置:" textSize="22sp" textColor="#210303" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="开启挂机:" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <Switch id="开启挂机" checked="false" h="*" w="*" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px" id="挂机时长布局" visibility="gone">
                    <text text="挂机时长(分钟):" textSize="16sp" h="*" w="*" gravity="left|center" layout_weight="1" />
                    <input id="挂机时长" inputType="number" hint="请输入0-30的数字,最大支持30" textSize="16sp" h="*" w="*" margin="0" bg="#ffffff" padding="15px 0 0 0" gravity="right|center" layout_weight="2" />
                </horizontal>
                <text text="技能设置:" textSize="16sp" textColor="#210303" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="佣兵一:" textColor="#210303" textSize="16sp" h="*" gravity="left|center" layout_weight="1" />
                    <spinner id="佣兵一首选项" entries="" h="*" w="auto" gravity="left|center" layout_weight="1" popupBackground="#ffffff" />
                    <spinner id="佣兵一备选项" entries="" h="*" w="auto" gravity="left|center" layout_weight="1" popupBackground="#ffffff" />
                </horizontal>
                <horizontal h="80px">
                    <text text="随机技能:" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <Switch id="随机技能" checked="false" h="*" w="*" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px">
                    <text text="技能优先目标:" textSize="16sp" h="*" w="450px" gravity="left|center" layout_weight="1" />
                    <spinner id="技能优先目标" entries="" h="*" w="*" gravity="left|center" layout_weight="2" popupBackground="#ffffff" />
                </horizontal>
                <text text="宝藏设置:" textSize="16sp" textColor="#210303" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="优先匹配宝藏:" textSize="16sp" h="*" w="450px" gravity="left|center" layout_weight="1" />
                    <input id="优先匹配宝藏" hint="请填写宝藏关键字,多个使用|隔开,按顺序进行优先匹配" textSize="16sp" inputType="text" h="*" w="*" margin="0" bg="#ffffff" padding="15px 0 0 0" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px">
                    <text text="匹配宝藏阈值:" textSize="16sp" h="*" w="450px" gravity="left|center" layout_weight="1" />
                    <input id="匹配宝藏阈值" inputType="number" hint="请输入0-255的数字,最大支持255,不填则默认按65取值" textSize="16sp" h="*" w="*" margin="0" bg="#ffffff" padding="15px 0 0 0" gravity="left|center" layout_weight="2" />
                </horizontal>
            </vertical>
        </card>,
        ui.uiView,
        true
    )

}

// 初始化方法
serviceUi.initUiSetting = () => {
    // 注释return 可查看示例效果
    return;
    // 初始化xml内容
    serviceUi.initXml()
    // 特殊的处理
    let 旅行点 = serviceConstant.旅行点
    // 旅行点下拉框处理
    let 旅行点Arr = Object.keys(旅行点)
    // 初始化旅行点下拉框
    utils.initSelect('select旅行点', 旅行点Arr, -1, (serviceStorage.get("select旅行点") || "贫瘠之地"), (textContent, parent, view, position, id, UIID) => {
        let cur悬赏列表名称Arr = 旅行点[textContent].map(item => item.name)
        // 初始化悬赏下拉框
        let select悬赏 = serviceStorage.get("select悬赏") || 旅行点[textContent][0].name
        utils.initSelect('select悬赏', cur悬赏列表名称Arr, -1, select悬赏)
    })

    let serviceSettingRange = serviceConstant.serviceSettingRange
    // 难度单选框处理
    let 难度Item = serviceSettingRange['select难度'].find(item => item.name === (serviceStorage.get("select难度") || "普通"))
    let 难度ItemId = 难度Item ? 难度Item.id : serviceSettingRange['select难度'][0].id
    // 初始化难度单选框
    utils.initRadioGroup("select难度", serviceSettingRange['select难度'], 难度ItemId, (textContent, parent, dataList, checkedId) => { })


    // 队伍模式单选框处理
    let 队伍模式Item = serviceSettingRange['队伍selectModel'].find(item => item.name === (serviceStorage.get("队伍selectModel") || "固定识图"))
    let 队伍模式ItemId = 队伍模式Item ? 队伍模式Item.id : serviceSettingRange['队伍selectModel'][0].id
    // 初始化队伍模式单选框
    utils.initRadioGroup("队伍selectModel", serviceSettingRange['队伍selectModel'], 队伍模式ItemId, (textContent, parent, dataList, checkedId) => {
        if (textContent === "固定识图") {
            ui.固定识图.attr('visibility', 'visible')
            ui.自定义识字.attr('visibility', 'gone')
            // 初始化选择队伍下拉框
            let select队伍 = serviceStorage.get("select队伍") || "刷图"
            utils.initSelect('select队伍', serviceConstant.队伍列表, -1, select队伍)
        } else if (textContent === "自定义识字") {
            ui.固定识图.attr('visibility', 'gone')
            ui.自定义识字.attr('visibility', 'visible')
        }
    })

    // 初始化挂机开关change事件
    utils.switchChangeEvent("开启挂机", (checked) => {
        ui.挂机时长布局.attr('visibility', checked ? 'visible' : 'gone')
    })


    if (ui.挂机时长) {
        ui.挂机时长.addTextChangedListener(new android.text.TextWatcher({
            onTextChanged(text) {
                let 挂机时长值 = Number(text)
                if (挂机时长值 < 0) {
                    挂机时长值 = 0
                } else if (挂机时长值 > 30) {
                    挂机时长值 = 30
                }
                if (Number(text) < 0 || Number(text) > 30) {
                    ui.挂机时长.attr("text", 挂机时长值)
                }
            }
        }))
    }

    if (ui.匹配宝藏阈值) {
        ui.匹配宝藏阈值.addTextChangedListener(new android.text.TextWatcher({
            onTextChanged(text) {
                let 匹配宝藏阈值val = Number(text)
                if (匹配宝藏阈值val < 0) {
                    匹配宝藏阈值val = 0
                } else if (匹配宝藏阈值val > 255) {
                    匹配宝藏阈值val = 255
                }
                if (Number(text) < 0 || Number(text) > 255) {
                    ui.匹配宝藏阈值.attr("text", 匹配宝藏阈值val)
                }
            }
        }))
    }


    utils.initSelect('连续同一页面次数', serviceSettingRange['连续同一页面次数'], -1, (serviceStorage.get("连续同一页面次数") || 15), () => { })
    utils.initSelect('连续无匹配页面次数', serviceSettingRange['连续无匹配页面次数'], -1, (serviceStorage.get("连续无匹配页面次数") || 15), () => { })


    // 初始化佣兵技能
    utils.initSelect('佣兵一首选项', serviceSettingRange['佣兵一首选项'], -1, (serviceStorage.get("佣兵一首选项") || "技能一"), () => { })
    utils.initSelect('佣兵一备选项', serviceSettingRange['佣兵一备选项'], -1, (serviceStorage.get("佣兵一备选项") || "技能二"), () => { })


    // 初始化技能优先目标
    utils.initSelect('技能优先目标', serviceSettingRange['技能优先目标'], -1, (serviceStorage.get("技能优先目标") || serviceSettingRange['技能优先目标'][0]), () => { })


    setTimeout(()=>{
        // 读取缓存数据
        utils.getUICacheData(serviceConstant.serviceSettingKey, serviceStorage)

        let 开启挂机 = serviceStorage.get("开启挂机") || false
        ui.挂机时长布局.attr('visibility', 开启挂机 ? 'visible' : 'gone')
    },500)
}


// 设置ui缓存
serviceUi.setUICacheData = () => {
    // 设置缓存数据
    utils.setUICacheData(serviceConstant.serviceSettingKey, serviceStorage)
}

// 读取ui缓存
serviceUi.getUICacheData = () => {
    // 读取缓存数据
    utils.getUICacheData(serviceConstant.serviceSettingKey, serviceStorage)
}
module.exports = serviceUi
// 配置类
let config = require("./common/config.js")
// 工具类
let utils = require("./common/utils.js")
// 导入公共常量类
let commonConstant = require('./common/commonConstant.js')
// 导入业务ui
let serviceUi = require('./serviceUi.js')
// 导入常量类
let serviceConstant = require('./common/serviceConstant.js')
// 公共储存对象
var commonStorage = storages.create("zjh336.cn" + config.commonScriptKey);
// 业务储存对象
var serviceStorage = storages.create("zjh336.cn" + config.serviceScriptKey);

const resources = context.getResources();
const densityDpi = resources.getDisplayMetrics().densityDpi;
const density = resources.getDisplayMetrics().density;

// 工具箱保活
let notCloseSourceArr = ['/data/user/0/com.zjh336.cn.tools/files/project/runScript.js', '/data/user/0/com.zjh336.cn.tools/files/project/main.js']

ui["屏幕宽度"].attr("text", device.width)
ui["屏幕高度"].attr("text", device.height)
ui["DPI"].attr("text", densityDpi)
$debug.gc()

let deviceUUID = commonStorage.get('deviceUUID')
if (!deviceUUID) {
    // 安卓10及以上 取androidId   10以下 取IMEI
    deviceUUID = config.SDK_API_VERSION > 28 ? device.getAndroidId() : device.getIMEI()
    commonStorage.put("deviceUUID", deviceUUID)
}

threads.start(function () {
    while (true) {
        text("立即开始").click()
    }
});

threads.start(function () {
    try {
        if (!images.requestScreenCapture({orientation:utils.getOrientation()})) {
            log("请求截图失败")
        } else {
            log("请求截图成功")
        }
    } catch (error) {
        console.error("请求截图权限错误", error)
    }
})



// 设备信息
let deviceInfo = {}
// 应用信息
let appInfo = {}
// 最新版本信息
let newVersionInfo = {}

let obj = {}
let runFm
let FloatMenu
// 消息悬浮窗
let messageFloaty

// 设置默认包名  按需改成自己应用的
let curAppDefaultPackage = "com.zjh336.cn.tools";

// 当前程序包名 
let curAppPackage = auto.service ? currentPackage() : curAppDefaultPackage;

// 二值化工具对象
let imgaeTools


// 初始化菜单浮窗
function initFloatMenuFun() {
    if (!runFm) {
        runFm = new FloatMenu();
    }
    let itemRun = runFm.addItem('运行脚本')
    //设置item背景色
    itemRun.setColors('#7a0066ff', '#7aff2851');
    //设置item图标颜色
    itemRun.setTints('#ffffff');
    //设置item图标
    itemRun.setIcons('file://res/icon/play.png', 'file://res/icon/stop.png');
    //设置按钮样式为 复选框样式
    itemRun.setStyle(FloatMenu.STYLE_ITEM_CHECKBOX);
    //设置背景样式
    itemRun.setRadius(10).setStroke(2, '#000000');
    //设置item选中
    itemRun.setChecked(false);
    itemRun.onClick((view) => {
        if (view.checked) {
            toastLog("开始运行")
            threads.start(() => {
                engines.execScriptFile("./module/mainScript.js");
            })
        } else {
            events.broadcast.emit("mainHideLog", "");
            events.broadcast.emit("closeFloat", "");
			let items = runFm.getViewUtil().getItemViews()
			let logButton = items.find(item => item.name === "日志")
			if (logButton) {
				logButton.setChecked(false);
			}
            toastLog("停止中")
            setTimeout(() => {
                const myScript = engines.myEngine()
                const all = engines.all()
                all.forEach(item => {
                    if (item.id !== myScript.id && !notCloseSourceArr.includes(String(item.source))) {
                        item.forceStop()
                    }
                });
                // 读取业务缓存数据
                serviceUi.getUICacheData()
                toastLog("已停止运行")
            }, 500)
        }
    });

    runFm.addItem('状态栏')
        .setColors('#3aff2851', '#7aff2851')
        .setIcons('file://res/icon/status.png')
        //设置设置背景圆角
        .setRadius(10)
        //设置item图标颜色
        .setStyle(FloatMenu.STYLE_ITEM_CHECKBOX)
        //设置描边属性
        .setStroke(2, '#000000')
        .setChecked(false)
        .onClick(view => {
            if (view.checked) {
                // 初始状态栏浮窗
                initStatusFloat()
            } else {
                // 销毁状态栏浮窗
                destoryStatusFloat()
            }
        });

    runFm.addItem('工具箱')
        .setColors('#3aff2851')
        .setIcons('file://res/icon/tools.png')
        //设置设置背景圆角
        .setRadius(10)
        //设置描边属性
        .setStroke(2, '#000000')
        .onClick(view => {
            // 收起悬浮菜单
            runFm.setState(1)
            dialogs.select("请选择要使用的工具", ["CPU、GPU、内存监控信息"], (item) => {
                if (item === 0) {
                    threads.start(() => {
                        setTimeout(() => {
                            try {
                                // 执行脚本
                                engines.execScriptFile("./module/checkInfoFloat.js");
                            } catch (error) {
                                // console.error("执行测试脚本措施", error)
                            }
                        }, 500)
                    })
                }
            })
        });

    runFm.addItem('日志')
        .setColors('#3aff2851', '#7aff2851')
        .setIcons('file://res/icon/log.png')
        //设置设置背景圆角
        .setRadius(10)
        //设置描边属性
        .setStroke(2, '#000000')
        //设置item图标颜色
        .setStyle(FloatMenu.STYLE_ITEM_CHECKBOX)
        .setChecked(false)
        .onClick(view => {
            if (itemRun.checked) {
                if (view.checked) {
                    events.broadcast.emit("mainShowLog", "");
                } else {
                    events.broadcast.emit("mainHideLog", "");
                }
            } else {
                toastLog("请启动主程序后重试！")
            }
        });

    runFm.addItem('退出')
        .setIcons('ic_exit_to_app_black_48dp')
        .setColors('#fafa00')
        .onClick(view => {
            if (runFm) {
                runFm.close();
                floaty.closeAll();
            }
            toastLog("停止中")
            setTimeout(() => {
                ui.startScript.attr("text", "启动脚本")
                stopScriptFun()
                launchPackage(curAppPackage)
            }, 500)
        });

    //监听item点击事件
    runFm.on('item_click', view => {
        // toastLog(`item_click ${view.name} 关闭菜单`);
        //返回false 关闭菜单
        return false;
    });
    /** 扇形悬浮菜单样式 */
    //设置控件与logo间距
    runFm.config.all_item_gap = 80;
    // 设置菜单样式为扇形
    runFm.setMenuStyle(FloatMenu.TYPE_MENU_CIRCULAR);
    //显示悬浮球 自行判断悬浮窗权限
    runFm.show();
}

// 销毁菜单浮窗
function destoryFloatMenu() {
    if (runFm) {
        runFm.close();
    }
    runFm = null
    destoryStatusFloat()
}

// 初始化状态栏浮窗
function initStatusFloat() {
    try {
        if (messageFloaty) {
            messageFloaty.close()
        }
        messageFloaty = floaty.rawWindow(
            <frame bg="#3b3737" padding="0" alpha="0.5" >
                <horizontal h='30' w="*" id="container">
                    <text textColor='#ffffff' w='300' h='auto' padding='0' id='messageText' gravity='center' layout_gravity="center" />
                </horizontal>
            </frame>
        );
        //记录按键被按下时的触摸坐标
        var x = 0,
            y = 0;
        //记录按键被按下时的悬浮窗位置
        var windowX, windowY;
        messageFloaty.container.setOnTouchListener(function (view, event) {
            switch (event.getAction()) {
                case event.ACTION_DOWN:
                    x = event.getRawX();
                    y = event.getRawY();
                    windowX = messageFloaty.getX();
                    windowY = messageFloaty.getY();
                    return true;
                case event.ACTION_MOVE:
                    //移动手指时调整悬浮窗位置
                    messageFloaty.setPosition(windowX + (event.getRawX() - x),
                        windowY + (event.getRawY() - y));
                    return true;
            }
            return true;
        });
        if (!messageFloaty) {
            return
        }
        messageFloaty.messageText.setText("点击按钮开始")
    } catch (error) {
    }
}

// 销毁状态栏浮窗
function destoryStatusFloat() {
    if (messageFloaty) {
        messageFloaty.close()
    }
}

// 开始脚本
function startScriptFun(callback) {
    const btnContent = ui.startScript.attr("text")
    toast(btnContent)
    const afterBtnContent = "启动脚本" === btnContent ? "停止脚本" : "启动脚本"
    ui.startScript.attr("text", afterBtnContent)
    if (btnContent === "启动脚本") {
        // 初始化浮窗菜单
        initFloatMenuFun()
        if (callback) {
            callback()
        }
    } else {
        stopScriptFun()
    }
}

// 停止脚本
function stopScriptFun() {
    // 销毁菜单浮窗
    destoryFloatMenu()
    const myScript = engines.myEngine()
    const all = engines.all()
    all.forEach(item => {
        if (item.id !== myScript.id && !notCloseSourceArr.includes(String(item.source))) {
            item.forceStop()
        }
    });
    // 读取业务缓存数据
    serviceUi.getUICacheData()
    mainScript = null
}
// 已初始化全选
let alreadyInitCheckAll = false
// 初始化ui设置
function initUiSetting() {
    // 初始化公共ui设置 
    ui.inflate(
        <card marginBottom="40px" contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
            <vertical>
                <text text="公共参数设置:" textSize="22sp" textColor="#210303" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="选择业务" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <spinner id="select业务" entries="" h="*" w="*" gravity="left|center" layout_weight="2" popupBackground="#ffffff" />
                </horizontal>
                <horizontal h="80px">
                    <text text="自动运行:" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <Switch id="自动运行" checked="false" h="*" w="*" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px">
                    <text text="调试模式:" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <Switch id="debugModel" checked="false" h="*" w="*" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px" id="调试延时" visibility="gone">
                    <text text="调试延时:" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                    <input id="debugSleep" hint="请填写延时毫秒数,建议1000" h="*" w="*" margin="0" textSize="16sp" padding="15px 0 0 0" bg="#ffffff" inputType="text" gravity="left|center" layout_weight="2" />
                </horizontal>
                <vertical h="auto" id="调试页面" visibility="gone">
                    <horizontal h="80px">
                        <text text="调试页面:" textSize="16sp" h="*" w="400px" gravity="left|top" padding="0 10px 0 0" layout_weight="1" />
                        <checkbox id="全选" text="全选" h="*" w="*" checked="false" gravity="left|center" layout_weight="2" />
                    </horizontal>
                    <vertical id="debugPageView" h="*" w="*" gravity="left|center">
                    </vertical>
                </vertical>
            </vertical>
        </card>,
        ui.uiView,
        true
    )
	
	
	// 运行设置
    ui.inflate(
        <card contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
            <vertical>
                <text text="运行设置:" textSize="22sp" textColor="#210303" marginBottom="5px" />
                <text text="超过次数重启游戏:" textSize="16sp" textColor="#210303" marginTop="10px" marginBottom="5px" />
                <horizontal h="80px">
                    <text text="连续同一页面次数:" textSize="16sp" h="*" w="600px" gravity="left|center" layout_weight="1" />
                    <spinner id="连续同一页面次数" textSize="16sp" h="*" w="*" margin="0" popupBackground="#ffffff" gravity="left|center" layout_weight="2" />
                </horizontal>
                <horizontal h="80px">
                    <text text="连续无匹配页面次数:" textSize="16sp" h="*" w="650px" gravity="left|center" layout_weight="1" />
                    <spinner id="连续无匹配页面次数" textSize="16sp" h="*" w="*" margin="0" popupBackground="#ffffff" gravity="left|center" layout_weight="2" />
                </horizontal>
            </vertical>
        </card>,
        ui.uiView,
        true
    )
	
	// 公共参数值域
    let commonSettingRange = commonConstant.commonSettingRange
	utils.initSelect('连续同一页面次数', commonSettingRange['连续同一页面次数'], -1, (commonStorage.get("连续同一页面次数") || 15), () => { })
    utils.initSelect('连续无匹配页面次数', commonSettingRange['连续无匹配页面次数'], -1, (commonStorage.get("连续无匹配页面次数") || 15), () => { })

	

    let 文字识别插件 = commonStorage.get("文字识别插件") || "谷歌"
    utils.initOcr(文字识别插件)

    // 调用业务ui初始化
    serviceUi.initUiSetting()
    // 调试模式监听
    utils.switchChangeEvent("debugModel", (checked) => {
        ui["调试延时"].attr("visibility", checked ? "visible" : "gone")
        ui["调试页面"].attr("visibility", checked ? "visible" : "gone")
        if (checked) {
            let select业务 = commonStorage.get("select业务") || "体力"
            initDebugPageCheckBox(select业务)

            if (ui["全选"] && !alreadyInitCheckAll) {
                alreadyInitCheckAll = true
                ui["全选"].on("check", (checked) => {
                      // 获取当前业务 
                    let select业务1 =  ui["select业务"].getSelectedItem() || "体力"
                    // 当前业务 的匹配页面 key数组
                    let curMatchingPageKey1 = utils.getCurPageSettingKey(select业务1)
                    // 参与匹配的key
                    curMatchingPageKey1.forEach(key => {
                        if(ui["debugPage_" + key]){
                            ui["debugPage_" + key].attr("checked", checked)
                        }
                    })
                })
            }
        }
    })

 
    // 初始化业务
    utils.initSelect('select业务', commonSettingRange['select业务'], -1, (commonStorage.get("select业务") || "体力"), (textContent,parent,view,position,id ,UIID) => {
        // 是调试模式
        let isDebugModel = ui.debugModel.attr("checked")
        if(isDebugModel){
            // 切换业务类型 重新渲染调试页面
            initDebugPageCheckBox(textContent)
        }
     })


    // 读取公共缓存数据
    utils.getUICacheData(commonConstant.commonSettingKey, commonStorage)

    setTimeout(()=>{
        // 读取调试页面缓存
        utils.getDebugPageSettingUICache()
    },500)
}

// 初始化debug模式 页面复选框
function initDebugPageCheckBox(select业务){
    // 清空全部视图
    ui.debugPageView.removeAllViews()
    // 当前业务 的匹配页面 key数组
    let curMatchingPageKey = utils.getCurPageSettingKey(select业务)
    console.log(curMatchingPageKey)
    // 页面复选框xml
    let pageSettingCheckBoxXml = '<vertical>';

    curMatchingPageKey.forEach((item, index) => {
        let temp = (index + 1) % 2
        if (temp === 1) {
            pageSettingCheckBoxXml += '<horizontal><checkbox id="debugPage_' + item + '" text="' + item + '"/>'
            if (index === curMatchingPageKey.length - 1) {
                pageSettingCheckBoxXml += "</horizontal>"
            }
        } else if (temp === 0) {
            pageSettingCheckBoxXml += '<checkbox id="debugPage_' + item + '" text="' + item + '"/>'
            pageSettingCheckBoxXml += "</horizontal>"
        }
    })
    pageSettingCheckBoxXml += '</vertical>'
    // 初始调试页面 
    ui.inflate(pageSettingCheckBoxXml, ui.debugPageView, true)
}

// 初始化方法
obj.init = function (initFloatMenu) {
    // 引入悬浮菜单类
    FloatMenu = initFloatMenu
    // 初始化ui设置
    initUiSetting()

    let 自动运行 = commonStorage.get("自动运行") || false
    if (自动运行) {
        toastLog("自动运行")
        ui.run(() => {
            startScriptFun(() => {
                setTimeout(() => {
                    let items = runFm.getViewUtil().getItemViews()
                    let 运行脚本 = items.find(item => item.name === "运行脚本")
                    if (运行脚本) {
                        运行脚本.setChecked(true)
                        console.log("运行主程序脚本")
                    }
                    let 状态栏 = items.find(item => item.name === "状态栏")
                    if (状态栏) {
                        状态栏.setChecked(true)
                        console.log("开启状态栏")
                    }
                }, 1000)
            })
        })
    }

    // 开始脚本按钮
    ui.startScript.on("click", () => {
        startScriptFun()
    })
    // 保存设置按钮
    ui.saveSetting.on("click", () => {
        // 设置公共缓存数据
        utils.setUICacheData(commonConstant.commonSettingKey, commonStorage)
        // 设置ui缓存数据
        serviceUi.setUICacheData()
        // 设置调试页面缓存
        utils.setDebugPageSettingUICache()
        toastLog("保存成功！")
    })
    // 加载设置按钮
    ui.loadSetting.on("click", () => {
        // 读取公共缓存数据
        utils.getUICacheData(commonConstant.commonSettingKey, commonStorage)
        // 读取业务缓存数据
        serviceUi.getUICacheData()
        // 读取调试页面缓存
        utils.getDebugPageSettingUICache()
        toast("载入成功！")
    })
    // 更新状态栏监听
    events.broadcast.on("updateMessageFloaty", function (key, value) {
        ui.run(function () {
            try {
                if (messageFloaty && messageFloaty[key]) {
                    messageFloaty[key].setText(value)
                }
            } catch (error) {
            }
        })
    });
    // 监听自定义toast方法
    events.broadcast.on("showTotast", function (name) {
        utils.toast(name);
    });
}

module.exports = obj;
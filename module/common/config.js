let config = {}
// 公共脚本key
config.commonScriptKey = "华仔AutoJs游戏脚本脚手架common";
// 业务脚本key
config.serviceScriptKey = "华仔AutoJs游戏脚本脚手架";
// appKey
config.appKey = "华仔AutoJs游戏脚本脚手架"
// 分辨率 以竖屏标准看
config.screenWidth = 1080
config.screenHeight = 1920
// https://blog.csdn.net/wangsheng5454/article/details/117119402
// 安卓API版本  29 安卓10
config.SDK_API_VERSION = android.os.Build.VERSION.SDK_INT

module.exports = config
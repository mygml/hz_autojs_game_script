// 公共设置key
let commonSettingKey = [
    { key: 'debugModel', type: "开关" },
    { key: 'debugSleep', type: "输入框" },
    { key: 'webSocketLog', type: "开关" },
    { key: '自动运行', type: "开关" },
	{ key: '连续同一页面次数', type: "下拉框" },
    { key: '连续无匹配页面次数', type: "下拉框" },
    { key: "select业务", type: "下拉框" }
]

// 公共设置值域
let commonSettingRange = {
    "select业务": ["体力", "竞技场"], // 业务名称值域列表
    "连续同一页面次数": [15, 20, 25, 30, 35, 40],
    "连续无匹配页面次数": [15, 20, 25, 30, 35, 40]
}


// 坐标偏移系数
let positionOffset = {
    "1080_1920": {// 标准分辨率不偏移
        "offsetX": 0,
        "offsetY": 0
    },
    "1080_2400": {
        "offsetX": 5,
        "offsetY": 0
    }
}

// 第一套匹配页面 命名规则 pageSetting_ + 业务名称
let pageSetting_体力 = {
    "今日奖励": {
        "1080_1920": {
            "relation": { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" },
            "analysisChart": [{ "position": [809, 92, 1148, 175], "threshold": 180, "maxVal": 255, "context": "今日奖励", "matchingType": "contains", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "今日奖励" }],
            "multipleColor": [],
            "multipleImg": []
        }
    },
    "菜单栏": {
        "1080_1920": {
            "relation": { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" },
            "analysisChart": [{ "position": [299, 86, 1583, 254], "threshold": 180, "maxVal": 255, "context": "主页", "matchingType": "contains", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "主页" }],
            "multipleColor": [],
            "multipleImg": [{ "position": [315, 80, 613, 265], "threshold": 180, "maxVal": 255, "pathName": "./res/1080_1920/菜单栏_主页图标.png", "imgThreshold": 0.8, "bigScale": 1, "smallScale": 1, "featuresThreshold": 0.8, "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "菜单栏_主页图标" }]
        }
    },
    "主页": {
        "1080_1920": {
            "relation": { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" },
            "analysisChart": [],
            "multipleColor": [],
            "multipleImg": [{ "position": [0, 0, 228, 100], "threshold": 180, "maxVal": 255, "pathName": "./res/1080_1920/主页_设置图标.png", "imgThreshold": 0.8, "bigScale": 1, "smallScale": 1, "featuresThreshold": 0.8, "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "主页_设置图标" }]
        }
    },
    "水晶潭": {
        "1080_1920": {
            "relation": { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" },
            "analysisChart": [{ "position": [1481, 920, 1879, 1048], "threshold": 180, "maxVal": 255, "context": "个水晶", "matchingType": "contains", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "开启N个水晶" },{ "position": [704,146,1231,242], "threshold": 180, "maxVal": 255, "context": "个水晶", "matchingType": "contains", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "个水晶" }],
            "multipleColor": [],
            "multipleImg": []
        }
    },
    "水晶列表": {
        "1080_1920": {
            "relation": { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" },
            "analysisChart": [{ "position": [301, 153, 1635, 264], "threshold": 180, "maxVal": 255, "context": "水晶碎片", "matchingType": "contains", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "水晶碎片" }],
            "multipleColor": [],
            "multipleImg": []
        }
    }
}

// 第二套匹配页面
let pageSetting_竞技场 = {
    "竞技场": {
        "1080_2400": {
            'relation': { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or", "nextKey": "选择队伍_待开始" },
            'analysisChart': [{ "position": [1101, 376, 1283, 422], "threshold": 60, "maxVal": 255, "context": "锁定", "matchingType": "contains", "isOpenGray": 1, "isOpenThreshold": 1, "canvasMsg": "锁定" }],
            'multipleColor': [],
            'multipleImg': [{ "position": [1101, 376, 1283, 422], "threshold": 60, "maxVal": 255, "pathName": "./res/锁定队伍_锁定队伍.png", "imgThreshold": 0.8, "isOpenGray": 1, "isOpenThreshold": 1, "canvasMsg": "锁定队伍_锁定队伍" }]
        }
    }
}

// 业务操作参数
let serviceOperateParam = {
    "今日奖励": {
        "关闭按钮_多点颜色点击": {
            "1080_1920": { "position": [1355, 51, 1585, 244], "threshold": 60, "maxVal": 255, "color": "#727070", "colorOther": [[23, 2, "#312F2F"], [31, 3, "#727070"], [2, 22, "#312F2F"], [3, 30, "#6E6D6D"]], "colorThreshold": 26, "isOpenGray": false, "isOpenThreshold": false, "canvasMsg": "关闭按钮图标" }
        }
    },
    "主页": {
        "水晶就绪_识字点击": {
            "1080_1920": { "position": [784, 761, 1858, 954], "threshold": 60, "maxVal": 255, "context": "就绪", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "就绪" },
        },
        "体力标志_找图或特征": {
            "1080_1920": { "position": [804, 0, 1244, 112], "threshold": 25, "maxVal": 255, "pathName": "./res/1080_1920/主页_体力图标.png", "imgThreshold": 0.8, "bigScale": 1, "smallScale": 1, "featuresThreshold": 0.8, "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "体力图标" }
        }
    },
    "菜单栏": {
        "菜单栏_点击图片或特征": {
            "1080_1920": { "position": [300, 0, 500, 100], "threshold": 25, "maxVal": 255, "pathName": "./res/1080_1920/菜单栏_按钮图标.png", "imgThreshold": 0.8, "bigScale": 1, "smallScale": 1, "featuresThreshold": 0.8, "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "菜单栏按钮图标" }
        }
    },
    "水晶潭":{
        "开启N个水晶_识字点击":{
            "1080_1920": { "position": [1481, 920, 1879, 1048], "threshold": 60, "maxVal": 255, "context": "个水晶", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "个水晶" },
        },
        "打开N个_识字点击":{
            "1080_1920": { "position": [596,831,957,946], "threshold": 60, "maxVal": 255, "context": "打开", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "打开" },
        },
        "返回水晶密室_识字点击":{
            "1080_1920": { "position": [561,799,998,980], "threshold": 60, "maxVal": 255, "context": "返回水晶", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "返回水晶" },
        }
    },
    "水晶列表":{
        "水晶tab_识字点击":{
            "1080_1920": { "position": [287,137,743,288], "threshold": 60, "maxVal": 255, "context": "水晶", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "水晶" },
        },
        "开启_识字点击":{
            "1080_1920": { "position": [1256,292,1611,965], "threshold": 60, "maxVal": 255, "context": "开启", "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "开启" },
        },
        "返回_点击图片或特征":{
            "1080_1920": { "position": [0, 0, 245, 120], "threshold": 25, "maxVal": 255, "pathName": "./res/1080_1920/返回图标.png", "imgThreshold": 0.8, "bigScale": 1, "smallScale": 1, "featuresThreshold": 0.8, "isOpenGray": 0, "isOpenThreshold": 0, "canvasMsg": "返回图标" }
        }
    }
}


// 默认逻辑关系
const relationDeafult = { "total": "or", "analysisChart": "or", "multipleColor": "or", "multipleImg": "or" }


let constant = {
    'commonSettingKey': commonSettingKey,
    'commonSettingRange': commonSettingRange,
    'positionOffset': positionOffset,
    'serviceOperateParam': serviceOperateParam,
    'relationDeafult': relationDeafult,
    'pageSetting_体力': pageSetting_体力,
    'pageSetting_竞技场': pageSetting_竞技场
}
module.exports = constant
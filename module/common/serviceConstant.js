
let 旅行点 = {
    "贫瘠之地": [
        { name: "残暴的野猪人", "x": 636, "y": 377, matchingChart: "野猪" },
        { name: "空气元素", "x": 1021, "y": 396, matchingChart: "空气" },
        { name: "塞瑞娜-血羽", "x": 1410, "y": 395, matchingChart: "血羽" },
        { name: "药剂师赫布瑞姆", "x": 658, "y": 788, matchingChart: "师" },
        { name: "烈日行者傲蹄", "x": 1032, "y": 769, matchingChart: "日行" },
        { name: "巴拉克-科多班恩", "x": 1405, "y": 791, matchingChart: "巴拉克" },
        { name: "疯狂投弹者", "x": 748, "y": 372, matchingChart: "狂投" },
        { name: "腐烂的普雷莫尔", "x": 1218, "y": 408, matchingChart: "烂的" },
        { name: "尼尔鲁-火刃", "x": 850, "y": 750, matchingChart: "尼尔" },
        { name: "神秘奶牛", "x": 1280, "y": 780, matchingChart: "奶牛" }
    ],
    "费伍德森林": [
        { name: "猎手拉文", "x": 634, "y": 377, matchingChart: "拉文" },
        { name: "淬油之刃", "x": 1020, "y": 396, matchingChart: "油之" },
        { name: "堕落的守卫", "x": 1410, "y": 395, matchingChart: "守卫" },
        { name: "哈拉梵", "x": 658, "y": 788, matchingChart: "哈拉" },
        { name: "腐化的古树", "x": 1032, "y": 769, matchingChart: "化的" },
        { name: "魔王贝恩霍勒", "x": 1405, "y": 791, matchingChart: "王贝" }
    ],
    "冬泉谷": [
        { name: "雪爪", "x": 718, "y": 634, matchingChart: "雪爪" },
        { name: "雪人猎手拉尼尔", "x": 689, "y": 1020, matchingChart: "手拉" },
        { name: "雪崩", "x": 696, "y": 1415, matchingChart: "雪崩" },
        { name: "厄苏拉-风怒", "x": 302, "y": 670, matchingChart: "风" },
        { name: "冰吼", "x": 307, "y": 1032, matchingChart: "冰吼" },
        { name: "冰霜之王埃霍恩", "x": 298, "y": 1397, matchingChart: "之王" }
    ]
}

let 旅行点map = {
    "贫瘠之地": ["之地", "贫之"],
    "费伍德森林": ["森林"],
    "冬泉谷": ["泉谷"]
}

// 业务设置key 指定类型
let serviceSettingKey = [
    { key: 'select旅行点', type: "下拉框" },
    { key: 'select悬赏', type: "下拉框" },
    { key: 'select难度', type: "单选框" },
    { key: '队伍selectModel', type: "单选框" },
    { key: 'select队伍', type: "下拉框" },
    { key: '填写队伍', type: "输入框" },
    { key: '开启挂机', type: '开关' },
    { key: '挂机时长', type: "输入框" },
    { key: '技能优先目标', type: "下拉框" },
    { key: '优先匹配宝藏', type: "输入框" },
    { key: '匹配宝藏阈值', type: "输入框" },
    { key: '佣兵一首选项', type: "下拉框" },
    { key: '佣兵一备选项', type: "下拉框" },
    { key: '随机技能', type: "开关" }
]
// 业务设置值域
let serviceSettingRange = {
    "select难度": [{ id: 1, name: '普通' }, { id: 2, name: '英雄' }],
    "select队伍": ["刷图", "任务", "火焰", "冰霜", "自然", "奥术", "圣光", "野兽"],
    "佣兵一首选项": ['技能一', '技能二', '技能三', '技能四', '跳过'],
    "佣兵一备选项": ['技能一', '技能二', '技能三', '技能四', '跳过'],
    "队伍selectModel": [{ id: 1, name: '固定识图' }, { id: 2, name: '自定义识字' }],
    "技能优先目标": ["集火左侧", "暴击对象"]
}

let constant = {
    '旅行点': 旅行点,
    '旅行点map': 旅行点map,
    'serviceSettingRange': serviceSettingRange,
    'serviceSettingKey': serviceSettingKey
}
module.exports = constant
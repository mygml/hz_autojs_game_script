// 启动  app的名称
let appName = "Champions";
launchApp(appName);
// 注册控制台显示事件
events.broadcast.on("mainShowLog", () => {
    console.setSize(device.height / 5, device.width);
    console.setPosition(device.height * (7 / 8) - 100, 0)
    console.show()
});
events.broadcast.on("mainHideLog", () => {
    console.hide()
});
// 导入工具类
let utils = require('./module/common/utils.js')
events.on("exit", function () {
    // 更新状态栏内容
    events.broadcast.emit("updateMessageFloaty", 'messageText', '点击按钮开始');
    console.log("停止主程序脚本,结束运行");
    $debug.gc()
    utils.clearMemory()
});


// 无匹配次数
let notMatchingCount = 0
// 连续同一个页面次数
let samePageCount = 0;

// 导入业务常量类
let serviceConstant = require('./module/common/serviceConstant.js')

let config = require('./module/common/config.js')

let commonStorage = storages.create("zjh336.cn" + config.commonScriptKey);

let 文字识别插件 = commonStorage.get("文字识别插件") || "谷歌"
utils.initOcr(文字识别插件)

// 本地储存对象
var serviceStorage = storages.create("zjh336.cn" + config.serviceScriptKey);

// 当前页面
let curPage = ""

// 点击立即开始
threads.start(function () {
    while (true) {
        let click1 = text("立即开始").findOne(100)
        if(click1){
            click1.click()
        }
        let click2 = text("关闭应用").findOne(100)
        if(click2){
            click2.click()
        }
    }
});
sleep(1000)
try {
    images.stopScreenCapture()
    images.requestScreenCapture({orientation:utils.getOrientation()})
} catch (error) {
    if (commonStorage.get('debugModel')) {
        console.error("主程序请求截图错误", error)
    }
}

// 刷新截图权限
events.broadcast.on("refreshScreenCapture", () => {
    console.log("主程序接收指令,刷新截图权限")
    try {
        images.stopScreenCapture()
        images.requestScreenCapture({orientation:utils.getOrientation()})
        sleep(500)
    } catch (error) {
        console.error("刷新截图权限错误", error)
    }
});


// 获取当前页面
function getCurPage() {
    // 截全屏
    let allScreenImg = captureScreen();
    let select业务 = commonStorage.get("select业务")
    // 多条件匹配
    let curPage = utils.multipleConditionMatchingByServiceName(select业务, allScreenImg);
    return curPage;
}



/**
 * //区域找图或者区域特征匹配
 * utils.regionalFindImgOrFeatures
 * // 模板匹配或特征模板匹配
 * utils.regionalMatchTemplateOrMatchFeatures
 * 测试代码：
  sleep(1000)  
  let img = captureScreen()
  let targetImg = images.read('./res/1080_1920/主页_设置图标.png')
  let result = utils.regionalMatchTemplateOrMatchFeatures(img,targetImg,0,0,1920,1080,60,255,0.8,2,false,1,1,0.8,false,false,"测试代码")
  if(result){
    result.sortBy("left")
    toastLog(JSON.stringify(result.matches))
  }
  utils.recycleNull(img);
  utils.recycleNull(targetImg);
  toastLog(result);
 */

  
// 操作处理方法
function operateHandle(page) {
    let handlerFun = this["operate_" + page]
    if (handlerFun) {
        handlerFun()
    } else {
        console.warn("【" + page + "】未找到处理方法")
    }
}

function operate_今日奖励() {
    /**
     * 关闭窗口
     * 多点找色关闭图标
     * 区域坐标：1355,51,1585,244
     */
    let 点击关闭按钮 = utils.getServiceOperateParam('今日奖励', '关闭按钮_多点颜色点击');
    // 获取到了业务参数
    if (点击关闭按钮) {
        // 截全屏
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, colorThreshold, colorOther, imgThreshold, isOpenGray, isOpenThreshold } = 点击关闭按钮
        // 多点颜色点击
        utils.regionalClickColor2(img, position[0], position[1], position[2], position[3], threshold, maxVal, colorThreshold, colorOther, imgThreshold, isOpenGray, isOpenThreshold, () => {
        });
        // 回收图片
        utils.recycleNull(img);
    }
}


function operate_主页() {
    /**
     * 1、找就绪区域文字识别就绪
     * 2、找体力标志  区域找图或者区域特征匹配
     * 3、找到体力标志换算坐标 文字识别体力值
     */
    sleep(500)
    let 就绪识字点击 = utils.getServiceOperateParam('主页', '水晶就绪_识字点击');
    if (就绪识字点击) {
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 就绪识字点击
        // 就绪文字的坐标并点击
        utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { });
        utils.recycleNull(img);
    }
    sleep(500)
    let 找体力标志 = utils.getServiceOperateParam('主页', '体力标志_找图或特征');
    if (找体力标志) {
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, pathName, imgThreshold, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, canvasMsg } = 找体力标志
        // 读取特征
        let targetImg = images.read(pathName);
        // 查找体力结果
        let 体力result = utils.regionalFindImgOrFeatures(img, targetImg, position[0], position[1], position[2], position[3], threshold, maxVal, imgThreshold, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, canvasMsg);
        utils.recycleNull(img);
        utils.recycleNull(targetImg);
        // 已找到体力标志
        if (体力result && 体力result.x !== -1) {
            // 区域识别文字 体力值  x,y,x+230,y+60

            let img = captureScreen();
            let 体力文字 = utils.regionalAnalysisChart2(img, 体力result.x, 体力result.y, 体力result.x + 230, 体力result + 60, 60, 255, false, false, "体力区域");
            utils.recycleNull(img);
            // 体力结果数组
            let 体力Arr = 体力文字.split("/") || [0, 0]
            // 体力大于40 
            if (体力Arr[0] >= 40) {
                //  进入326 TODO
            }
        }
    }
}

function operate_水晶潭() {
    /**
     * 1、点击开启N个水晶
     * 2、打开多个
     * 3、点击返回水晶密室
     */
    sleep(1000)
    let 开启N个水晶识字点击 = utils.getServiceOperateParam('水晶潭', '开启N个水晶_识字点击');
    if (开启N个水晶识字点击) {
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 开启N个水晶识字点击
        // 开启水晶文字的坐标并点击
        utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { 
            // 点击空白处加速动画
            utils.randomClick(1783,501,1,true);

        });
        utils.recycleNull(img);
    }
    sleep(1000)
    let 打开N个识字点击 = utils.getServiceOperateParam('水晶潭', '打开N个_识字点击');
    if (打开N个识字点击) {
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 打开N个识字点击
        // 打开N个文字的坐标并点击
        utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { 
        });
        utils.recycleNull(img);
    }
    sleep(1000)
    let 返回水晶密室识字点击 = utils.getServiceOperateParam('水晶潭', '返回水晶密室_识字点击');
    if(返回水晶密室识字点击){
        sleep(200);
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 返回水晶密室识字点击
        // 开启水晶文字的坐标并点击
        utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { 
        });
        utils.recycleNull(img);
    }
}

function operate_水晶列表() {
    /**
     * 1、切换到水晶tab
     * 2、检测是否有水晶可以开启
     * 3、开启水晶
     * 4、返回主页
     */
    sleep(500)
    let 水晶tab识字点击 = utils.getServiceOperateParam('水晶列表', '水晶tab_识字点击');
    if (水晶tab识字点击) {
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 水晶tab识字点击
        // 水晶文字的坐标并点击
        utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { });
        utils.recycleNull(img);
    }
    sleep(500)
    // 滑动次数
    let swipeCount = 0;
    // 已找到开启标志
    let findFlag = false;
    let 开启识字点击 = utils.getServiceOperateParam('水晶列表', '开启_识字点击');
    // 未找到开启
    while(!findFlag){
        sleep(200)
        if (开启识字点击) {
            let img = captureScreen();
            // 解构参数
            let { position, threshold, maxVal, context, isOpenGray, isOpenThreshold } = 开启识字点击
            // 开启文字的坐标并点击
            utils.regionalClickText2(img, position[0], position[1], position[2], position[3], threshold, maxVal, context, isOpenGray, isOpenThreshold, () => { 
                findFlag = true;
            });
            utils.recycleNull(img);
        }
        sleep(500)
        if(findFlag){
            break;
        }
        // 累计次数
        swipeCount+=1;
        // 滑动 1377,863  =》 1353,388
        let bottomPoint = utils.convertXY(424,902);
        let topPoint = utils.convertXY(433,360);
        // 从下往上滑动
        swipe(bottomPoint.x,bottomPoint.y,topPoint.x,topPoint.y,600)
        // 超过次数停止寻找
        if(swipeCount > 10){
            // 返回主页
            let 点击返回图片或特征 = utils.getServiceOperateParam('水晶列表', '返回_点击图片或特征');
            // 获取到了业务参数
            if (点击返回图片或特征) {
                // 截全屏
                let img = captureScreen();
                // 解构参数
                let { position, threshold, maxVal, pathName, imgThreshold, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, canvasMsg } = 点击返回图片或特征
                // 调用区域点击图片
                utils.regionalClickImgOrFeatures(img, position[0], position[1], position[2], position[3], threshold, maxVal, imgThreshold, pathName, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, () => {  });
                // 回收图片
                utils.recycleNull(img);
            }
            break;
        }
    }
}




function operate_菜单栏() {
    /**
     * 1、找菜单栏图标  区域找图
     * 2、点击菜单栏图标  根据坐标点击/区域找图点击
     * 调试了好一段时间才发现有两个问题
     * 1、这里sleep延迟必须要，否则截图还是上一个，取不到
     * 2、300,0,500,100 坐标范围还是要大一点
     */
    sleep(2000)
    let 点击菜单栏图片或特征 = utils.getServiceOperateParam('菜单栏', '菜单栏_点击图片或特征');
    // 获取到了业务参数
    if (点击菜单栏图片或特征) {
        // 截全屏
        let img = captureScreen();
        // 解构参数
        let { position, threshold, maxVal, pathName, imgThreshold, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, canvasMsg } = 点击菜单栏图片或特征
        // 调用区域点击图片
        utils.regionalClickImgOrFeatures(img, position[0], position[1], position[2], position[3], threshold, maxVal, imgThreshold, pathName, bigScale, smallScale, featuresThreshold, isOpenGray, isOpenThreshold, () => {  });
        // 回收图片
        utils.recycleNull(img);
    }
}


// 更新状态栏内容
events.broadcast.emit("updateMessageFloaty", 'messageText', '状态栏：准备中');
sleep(2500)
// 清空文件夹
files.removeDir("/sdcard/autoJsAfterImg/")
// 开启线程 监测当前页面
threads.start(() => {
    let tempCurPage = ""
    while (true) {
        sleep(500)
        try {
            // 判断当前所在页面
            tempCurPage = getCurPage()
        } catch (error) {
            console.info("尝试重新请求截图权限")
            if (commonStorage.get('debugModel')) {
                console.info("获取页面方法错误：", error)
            }
            images.stopScreenCapture()
            images.requestScreenCapture({orientation:utils.getOrientation()})
        }
        if (tempCurPage) {
            // 页面判定相同
            if (curPage === tempCurPage) {
                // 计数
                samePageCount += 1;
            } else {
                // 重置计数
                samePageCount = 0
            }
            // 重新賦值当前页面
            curPage = tempCurPage
            // 更新状态栏内容
            events.broadcast.emit("updateMessageFloaty", 'messageText', '当前页：' + curPage);
            try {
                // 对应页面的操作处理
                operateHandle(tempCurPage)
            } catch (error) {
                if (commonStorage.get('debugModel')) {
                    console.error("操作处理方法错误：", error)
                }
            }
            // 重置未匹配次数
            notMatchingCount = 0

        } else {
            // 未匹配次数+1
            notMatchingCount += 1
        }
        $debug.gc()
        utils.clearMemory()

        if (commonStorage.get('debugModel')) {
            let memoryInfo = utils.getMemoryInfo()
            console.info(memoryInfo)
        }

        let 连续同一页面次数 = commonStorage.get("连续同一页面次数") || 15
        let 连续无匹配页面次数 = commonStorage.get("连续无匹配页面次数") || 15

        if (notMatchingCount > 连续无匹配页面次数 * 0.75) {
            console.warn("尝试重开截图权限")
            try {
                images.stopScreenCapture()
                images.requestScreenCapture({orientation:utils.getOrientation()})
            } catch (error) {
                if (commonStorage.get('debugModel')) {
                    console.error("主程序请求截图错误", error)
                }
            }
            sleep(1000)
        }

        if (notMatchingCount > 连续无匹配页面次数) {
            console.warn("未匹配次数已达" + 连续无匹配页面次数 + "次,尝试重启游戏")
            notMatchingCount = 0;
            utils.exitApp(appName, () => {
                // 启动
                launchApp(appName);
            })
        }

        if (samePageCount > 连续同一页面次数) {
            console.warn("连续同一页面次数已达" + 连续同一页面次数 + "次,尝试重启游戏")
            samePageCount = 0;
            utils.exitApp(appName, () => {
                // 启动
                launchApp(appName);
            })
        }

    }
})
//保持脚本运行
setInterval(() => {
    // toastLog("脚本保持运行")
}, 5000);

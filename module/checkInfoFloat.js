let utils = require('./module/common/utils.js')
var window = floaty.window(
    <frame gravity="center" w="auto" h='auto'  bg="#3b3737" padding="1" alpha="0.7">
        <text id="text" textSize="12sp" h="auto" w="auto" textColor="#ffffff" text="信息获取中"/>
    </frame>
);
window.setPosition(0,600);
window.exitOnClose();

window.text.click(() => {
    window.setAdjustEnabled(!window.isAdjustEnabled());
});

setInterval(() => {
    //对控件的操作需要在UI线程中执行
    ui.run(function() {
        window.text.setText(dynamicText());
    });
}, 1000);

function dynamicText() {
    return utils.getMonitorInfo();
}
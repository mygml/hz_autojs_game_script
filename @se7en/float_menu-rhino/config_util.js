

new function () {
    const { defineValue, isHorizontalScreen } = require('./modules/utils');
    const FloatMenu = require('./index');
    const ItemView = require('./widgets/item_view')

    /**
     * 悬浮球配置类
     * @param {FloatMenu} fm 
     */
    function ConfigUtil(fm) {
        this.fm = fm;
        init(this);
    }

    ConfigUtil.prototype = {
        /** 所有按钮大小 默认44dp (单位dp) */
        all_item_size: 44,
        /** 所有控件内边距 默认8dp (单位dp); */
        all_item_padding: 8,
        /** 所有控件圆角大小 默认-1 (单位:dp) */
        all_item_radius: -1,
        /** 控件间距 垂直水平样式代表的是每个控件的间距,扇形样式代表控件距离logo按钮的间距 */
        all_item_gap: 10,
        /** 所有控件点击缩放参数 0~1; */
        all_item_scale: 0.8,

        //动画参数
        /** view点击动画时间 */
        animator_duration_item: 230,
        /** logo显示动画时间 */
        animator_duration_show: 500,
        /** logo贴边动画时间 */
        animator_duration_margin: 500,
        /** 菜单展开动画时间 */
        animator_duration_open: 230,

        //其他参数
        /** 
         * 扇形菜单展开角度 
         * @author 贡献者: I'm zz 大佬
         */
        angle: 180,
        /** logo按钮相对于屏幕所在位置的垂直比例*/
        logoY: 0.5,
        /** logo按钮透明度 */
        logoAlpha: 0.5,
        /** 是否在左侧边缘 */
        isLeftMargin: true,

        //下方参数不要修改
        /** 是否正在播放动画 */
        isAnimatedStart: false,
        width: device.width,
        height: device.height,
        defineValue
    }

    /** @param {ConfigUtil} scope */
    function init(scope) {
        scope.defineValue('all_item_size', undefined, size => {
            updateAllItemVies(scope, view => view.setSize(size));
            scope.fm.getMenuWindow()._style_.updateMenuPosition();
            scope.fm.getMenuWindow()._style_.updateMenuWindow();
        });
        scope.defineValue('all_item_padding', undefined, padding => {
            updateAllItemVies(scope, view => view.setPaddings(padding));
            scope.fm.getMenuWindow()._style_.updateMenuPosition();
            scope.fm.getMenuWindow()._style_.updateMenuWindow();
        });
        scope.defineValue('all_item_radius', undefined, radius => {
            updateAllItemVies(scope, view => view.setRadius(radius));
            scope.fm.getMenuWindow()._style_.updateMenuPosition();
            scope.fm.getMenuWindow()._style_.updateMenuWindow();
        });
        scope.defineValue('all_item_gap', undefined, gap => {
            scope.fm.getMenuWindow()._style_.updateMenuPosition();
            scope.fm.getMenuWindow()._style_.updateMenuWindow();
        });
        scope.defineValue('isAnimatedStart', undefined, value => {
            let type = value ? 'animation_start' : 'animation_end';
            scope.fm.emitter.emit(type, value);
        });
        scope.defineValue('isLeftMargin', undefined, value => {
            updateAllItemVies(scope, view => view.attr('layout_gravity',
                (value ? 'left' : 'right') + '|center_vertical'
            ));
        });
        scope.defineValue('orientation', null, value => {
            scope.width = device[value ? 'height' : 'width'];
            scope.height = device[value ? 'width' : 'height'];
            if (scope.fm.logoWindow && scope.fm.logoWindow.window) {
                scope.fm.logoWindow.updateWindowPosition();
            }
        });
        scope.orientation = isHorizontalScreen();
    }

    /**
     * @callback ItemViewsCallback
     * @param {ItemView} view  
     */
    /**
     * @param {ConfigUtil} scope 
     * @param {ItemViewsCallback} callback 
     */
    function updateAllItemVies(scope, callback) {
        scope.fm.viewUtil.getItemViews().forEach(iv => callback(iv))
    }

    module.exports = ConfigUtil;
}
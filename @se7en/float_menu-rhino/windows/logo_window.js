

new function () {
    const { def, dp2px, status_bar_height, navigation_bar_height } = require('../modules/utils');
    const AnimationFloat = require('../anim/animation_foat');
    const FloatMenu = require('../index');
    const Window = require("./window");
    /**
     * @class
     * @augments Window   
     * @param {FloatMenu} fm
     * @returns {Window}
     */
    function LogoWindow(fm) {
        this.fm = fm;
        this.margin_animator = null;
        /** @type{AutoJs.FloatyRawWindow} */
        this.window = null;
        Window.call(this);
        init(this);
    }

    LogoWindow.prototype = {
        //悬浮窗创建事件
        onCreateWindow(window, cv) {
            this.window = window;
            this.cv = cv;
            ui.run(() => initWindow(this, window, cv));
        },
        onCreateView() {
            return createContentView(this);
        },
        show() {
            this.show_animator.reverse();
        },
        hide() {
            this.show_animator.start();
        },
        updateWindowPosition() {
            ui.run(() => initWindow(this, this.window, this.cv));
        }
    }
    LogoWindow.prototype = Object.assign(LogoWindow.prototype, Window.prototype);
    LogoWindow.prototype.constructor = LogoWindow;

    /**
     * @param {LogoWindow} lw 
     */
    function createContentView(lw) {
        let view = lw.fm.viewUtil.getLogoView();
        view.setAlpha(lw.fm.config.logoAlpha);
        return view;
    }

    /**
     * @param {LogoWindow} scope 
     * @param {AutoJs.FloatyRawWindow} window 
     */
    function initWindow(scope, window, cv) {
        let p = dp2px(scope.fm.config.all_item_padding + 3);
        let s = dp2px(scope.fm.config.all_item_size + 6);
        let w = scope.fm.config.width - s + p * 2;
        if (scope.fm.config.isLeftMargin) w = 0;
        //设置logo悬浮窗位置;
        window.setPosition(w - p, scope.fm.config.height * scope.fm.config.logoY);
        //更新menu悬浮窗位置;
        scope.fm.menuWindow._style_.updateMenuWindow();
        //onTouch
        let wx, wy, rx, ry, isMove;
        let lv = scope.fm.viewUtil.getLogoView();
        cv.setOnTouchListener((view, e) => {
            //如果动画正在播放 则跳过此次操作;
            if (scope.fm.config.isAnimatedStart) return true;
            switch (e.getAction()) {
                case e.ACTION_DOWN://手指按下
                    isMove = false;
                    rx = e.getRawX();
                    ry = e.getRawY();
                    wx = window.getX();
                    wy = window.getY();
                    lv.setAlpha(1);
                    break;
                case e.ACTION_MOVE://手指移动
                    if (!isMove && (Math.abs(e.getRawX() - rx) > 30 || Math.abs(e.getRawY() - ry) > 30)) {
                        isMove = true;
                    } else if (isMove && !scope.fm.isMenuOpen()) {
                        window.setPosition(wx + e.getRawX() - rx, wy + e.getRawY() - ry);
                    }
                    break;
                case e.ACTION_UP:
                    if (isMove && !scope.fm.isMenuOpen()) {
                        //播放贴边动画
                        scope.margin_animator.start();
                    } else if (!isMove) {
                        if (scope.fm.isMenuOpen()) {
                            //关闭菜单
                            scope.fm.setState(1);
                        } else {
                            //显示菜单
                            scope.fm.setState(2);
                        }
                    }
                    break;
            }
            return true;
        });
    }

    /**
     * @param {LogoWindow} lw 
     */
    function init(lw) {
        def(lw, '__window__', undefined, value => {
            log('window----', value)
            lw.onCreateWindow(this.__window__, this.view);
        });
        initMarginAnimation(lw);
        initShowAnimation(lw);
    }

    /** @param {LogoWindow} scope */
    function initMarginAnimation(scope) {
        let animator = new AnimationFloat(scope.fm);
        animator.setDuration(scope.fm.config.animator_duration_margin);
        animator.setInterpolator(new AnimationFloat.BounceInterpolator());
        let f, sx, sy, ex, ey, w, h, p, s, y;
        animator.onAnimationStart(() => {
            scope.fm.config.isAnimatedStart = true;
            sx = scope.window.getX();
            sy = scope.window.getY();
            p = dp2px(scope.fm.config.all_item_padding + 3);
            s = dp2px(scope.fm.config.all_item_size + 6);
            //计算在要贴边在哪个方向
            w = -scope.fm.config.width + s - p * 2;
            if (scope.fm.config.isLeftMargin = scope.fm.config.width / 2 > sx + s / 2) w = 0;
            //计算y轴是否超出屏幕范围或不可点击区域
            h = scope.fm.config.height - status_bar_height - navigation_bar_height;
            if (status_bar_height > sy) {
                y = -(status_bar_height * 2 - sy);
            } else if (h < sy + s) {
                y = sy + s - h;
            } else {
                y = 0;
            }
            ex = w + sx + p;
            ey = y;
            //设置logoView透明度
            scope.fm.viewUtil.getLogoView().setAlpha(scope.fm.config.logoAlpha);
        });
        animator.onAnimationEnd(() => {
            //记录logo当前位置比例
            scope.fm.config.logoY = scope.window.getY() / scope.fm.config.height;
            scope.fm.config.isAnimatedStart = false;
            //贴边动画播放完后 更新menu菜单位置
            scope.fm.menuWindow._style_.updateMenuWindow();
        })
        animator.onAnimatoinUpdate(animator => {
            f = animator.getAnimatedValue();
            scope.window.setPosition(sx - ex * f, sy - ey * f);
        });
        scope.margin_animator = animator;
    }

    /** @param {LogoWindow} scope */
    function initShowAnimation(scope) {
        let animator = new AnimationFloat(scope.fm);
        animator.setDuration(scope.fm.config.animator_duration_show);
        let f, w, iv;
        animator.onAnimationStart((animator, isReverse) => {
            iv = scope.fm.viewUtil.getLogoView();
            w = -dp2px(scope.fm.config.all_item_size + 6);
            if (!scope.fm.config.isLeftMargin)
                w = Math.abs(w);
            scope.fm.config.isAnimatedStart = true;
            if (isReverse) iv.setTranslationX(w);
            iv.attr('visibility', 'visible');
        });
        animator.onAnimationEnd(() => {
            scope.fm.config.isAnimatedStart = false;
        })
        animator.onAnimatoinUpdate(animator => {
            f = animator.getAnimatedValue();
            iv.setTranslationX(w * f);
        });
        scope.show_animator = animator;
    }


    module.exports = LogoWindow;
}
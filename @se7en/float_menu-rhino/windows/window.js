
new function () {
    const LayoutParams = android.view.WindowManager.LayoutParams;

    function Window() {
        this.running = false;
        // init(this);
    }

    Window.prototype = {
        init() {
            if (!this.isRunning()) {
                this.running = true;
                $ui.run(() => init(this));
            }
            return this;
        },
        isRunning() {
            return this.running;
        },
        close() {
            if (this.isRunning()) {
                this.running = false;
                this.window.close();
            }
        },
    }

    function init(scope) {
        initContentView.call(scope);
        initWindow.call(scope);
    }

    function initContentView() {
        this.view = this.onCreateView.call(this);
    }

    function initWindow() {
        if (!this.isRunning()) return;
        if (!$floaty.checkPermission()) {
            if ($ui.isUiThread()) {
                return $threads.start(initWindow.bind(this));
            } else {
                while (this.isRunning() && !$floaty.checkPermission()) {
                    sleep(1e3);
                }
                return $ui.post(initWindow.bind(this));
            }
        } else if (!$ui.isUiThread()) {
            return $ui.post(initWindow.bind(this));
        }
        //create window;
        this.running = true;
        let window = runtime.floaty.rawWindow(global, (context, parent) => {
            parent.addView(this.view);
            return this.view;
        });
        //post 获取window flags修改为全屏悬浮窗
        $ui.post(function () {
            let mWindow = getClassField(window, 'mWindow');
            let params = mWindow.getWindowLayoutParams();
            if (params) {
                //设置全屏悬浮窗 
                //22/05/21:修复悬浮窗背景为模糊的flag
                params.flags = params.flags | LayoutParams.FLAG_FULLSCREEN | LayoutParams.FLAG_LAYOUT_IN_SCREEN | LayoutParams.FLAG_LAYOUT_NO_LIMITS;
                mWindow.updateWindowLayoutParams(params);
            }
        });
        this.onCreateWindow(window, this.view);
    }

    /**
    * 获取类内部私有变量
    * @param {*} javaObject 
    * @param {*} name 
    */
    function getClassField(javaObject, name) {
        var field = javaObject.class.getDeclaredField(name);
        field.setAccessible(true);
        return field.get(javaObject)
    }

    module.exports = Window;
}



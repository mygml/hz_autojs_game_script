

new function () {
    const AnimationFloat = require("../anim/animation_foat");
    const FloatMenu = require("../index");
    const Window = require("./window");
    const { defineValue } = require("../modules/utils");
    //菜单样式
    const StyleMenu = require("../styles/__style_menu__");
    const HorizontalMenu = require('../styles/horizontal_menu');
    const CircularMenu = require("../styles/circular_menu");

    /**
     * @param {FloatMenu} fm 
     */
    function MenuWindow(fm) {
        this.fm = fm;
        /** @type {StyleMenu} */
        this._style_ = null;
        this.isOpen = false;
        /** @type {AutoJs.FloatyRawWindow} */
        this.window = null;
        Window.call(this, fm);
        init(this);
        this.style = MenuWindow.TYPE_MENU_HORIZONTAL;
    }

    //水平菜单 默认样式
    MenuWindow.TYPE_MENU_HORIZONTAL = HorizontalMenu;
    //垂直菜单
    // MenuWindow.TYPE_MENU_VERTICAL = VerticalMenu;
    //扇形菜单
    MenuWindow.TYPE_MENU_CIRCULAR = CircularMenu;

    MenuWindow.prototype = {
        //悬浮窗创建事件
        onCreateWindow(window, cv) {
            this.window = window;
            initWindow.call(this, window, cv);
        },
        onCreateView() {
            return createContentView(this);
        },
        open() {
            this.animator.start();
        },
        hide() {
            this.animator.reverse();
        },
        setStyle(style) {
            this.style = style;
        },
        getStyle() {
            return this.style;
        },
        isMenuOpen() {
            return this.isOpen;
        },
        defineValue
    }

    MenuWindow.prototype = Object.assign(MenuWindow.prototype, Window.prototype);
    MenuWindow.prototype.constructor = MenuWindow;

    /**
     * @param {MenuWindow} mw 
     */
    function createContentView(mw) {
        let view = mw.fm.viewUtil.getMenuViewGroup();
        return view;
    }

    /**
     * @param {*} window 
     * @param {*} parent 
     */
    function initWindow(window, cv) {
        window.setSize(-1, -2);
        initMenuAnimator(this);
    }

    /**@param {MenuWindow} mw */
    function initMenuAnimator(mw) {
        let animator = new AnimationFloat(mw.fm);
        animator.setDuration(mw.fm.config.animator_duration_open);
        animator.onAnimatoinUpdate(animator => {
            mw._style_.updateMenuAnimation(animator.getAnimatedValue());
        });
        animator.onAnimationStart(() => {
            mw.isOpen = !mw.isOpen;
            mw.fm.config.isAnimatedStart = true;
            if (mw.isOpen) {
                mw.fm.viewUtil.getMenuViewGroup().attr('visibility', 'visible');
                mw.fm.viewUtil.getLogoView().setAlpha(1);
            } else
                try {
                    mw.window.setTouchable(false);
                } catch (error) {
                }
        });
        animator.onAnimationEnd(() => {
            mw.fm.config.isAnimatedStart = false;
            if (mw.isOpen)
                mw.window.setTouchable(true);
            else {
                mw.fm.viewUtil.getMenuViewGroup().attr('visibility', 'invisible');
                mw.fm.viewUtil.getLogoView().setAlpha(mw.fm.config.logoAlpha);
            }
        });
        mw.animator = animator;
    }

    /**
     * @param {MenuWindow} mw 
     */
    function init(mw) {
        mw.defineValue('style', null, style => {
            mw._style_ = new style(mw.fm);
        });
    }

    module.exports = MenuWindow;
}
new function () {
    const FrameLayout = android.widget.FrameLayout;
    const FloatMenu = require('../index');
    const ItemView = require('../widgets/item_view');


    /**
     * ItemView 控件工具类
     * @param {FloatMenu} fm 
     */
    function ViewUtil(fm) {
        // mFloatMenu = fm;
        this.fm = fm;
        /**@type{Array.<ItemView>} */
        this.item_views = [];
        /** @type{ItemView} */
        this.logo_view = null;
        this.menu_view_group = null;
        init(this);
    }

    ViewUtil.prototype = {
        /**
         * 添加一个控件到菜单
         * @param {string} name 控件名称
         * @param {number=} index 可选参数 将控件添加到指定位置 默认-1 添加到最后
         * @returns {ItemView}
         */
        addItem(name, index) {
            return ui.run(() => {
                let iv = this.findView(name);
                if (iv) return iv;
                index = index || -1;
                iv = new ItemView(name, this.fm);
                this.menu_view_group.addView(iv, index);
                if (index === -1) index = this.item_views.length;
                this.item_views.splice(index, 0, iv);
                this.fm.menuWindow._style_.updateMenuPosition();
                //更新item重力位置
                iv.attr('layout_gravity',
                    (this.fm.config.isLeftMargin ? 'left' : 'right') + '|center_vertical'
                );
                return iv;
            });
        },

        /**
         * 指定控件是否存在
         * @param {string} name 
         */
        hasItem(name) {
            return !!this.findView(name);
        },

        /**
         * 从菜单移除一个控件
         * @param {*} name 
         */
        deleteItem(name) {
            let iv = this.findView(name);
            if (iv) {
                $ui.run(() => this.menu_view_group.removeView(iv));
                this.item_views = this.item_views.filter(w => w !== iv);
            }
        },

        /**
         * 移除菜单所有控件
         */
        deleteAllItem() {
            $ui.run(() => this.menu_view_group.removeAllViews());
            this.item_views.length = 0;
        },

        /**
         * 获取指定控件
         * @param {string} name 
         * @returns {ItemView|null} 控件或者null;
         */
        findView(name) {
            return $ui.run(() => this.menu_view_group.findViewWithTag(name));
        },

        /**
         * 获取所有控件集合 包括logo控件
         * @returns {Array.<ItemView>} 控件集合
         */
        getItemViews() {
            return this.item_views.concat(this.logo_view);
        },

        getLogoView() {
            return this.logo_view;
        },

        getMenuViewGroup() {
            return this.menu_view_group;
        }
    }

    function init(scope) {
        $ui.run(() => {
            initLogoView(scope);
            initMenuViewGroup(scope);
        });
    }

    /**
     * 
     * @param {ViewUtil} scope 
     */
    function initLogoView(scope) {
        scope.logo_view = new ItemView('logo', scope.fm);
        scope.logo_view.setIcons('file://res/icon/float.png').setColors('#fafafa').attr('visibility', 'invisible');
    }

    /**
     * 
     * @param {ViewUtil} scope 
     */
    function initMenuViewGroup(scope) {
        scope.menu_view_group = $ui.inflate(<frame visibility='invisible' />);
        scope.menu_view_group.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
    }

    module.exports = ViewUtil;
}
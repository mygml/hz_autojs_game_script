new function () {

    function StyleMenu(window) {
        this.positions = new Map();
        this.menuAnimator = null;
        this.window = window;
    }

    StyleMenu.prototype = {
        /**
         * 更新菜单悬浮窗
         */
        updateMenuWindow() {
            ui.run(this.onUpdateMenuWindow.bind(this));
        },
        /**
         * 更新菜单坐标
         */
        updateMenuPosition() {
            ui.run(this.onUpdateMenuPosition.bind(this));
        },
        /**
         * 更新菜单动画
         */
        updateMenuAnimation(f) {
            ui.run(this.onUpdateMenuAnimation.bind(this, f));
        },
    }

    module.exports = StyleMenu;
}


new function () {
    const { dp2px } = require("../modules/utils");
    const StyleMenu = require("./__style_menu__");
    const ItemView = require("../widgets/item_view");
    const FloatMenu = require("../index")

    /**
     * 扇形菜单样式
     * @param {FloatMenu} fm 
     */
    function VerticalMenu(fm) {
        this.fm = fm;
        StyleMenu.call(this);
    }

    VerticalMenu.prototype = {
        onUpdateMenuPosition() { //更新itemView坐标
            this.positions = createItemViewPosition(this);
        },
        onUpdateMenuWindow(window) {

        },
        onUpdateMenuAnimation(animator) {

        }
    }

    VerticalMenu.prototype = Object.assign(VerticalMenu.prototype, StyleMenu.prototype);
    VerticalMenu.prototype.constructor = VerticalMenu;

    /**
     * 创建Item坐标
     * @param {VerticalMenu} scope 
     */
    function createItemViewPosition(scope) {
        let items = this.fm.viewUtil.item_views;
        let len = items.length;
        let size = dp2px(scope.fm.config.all_item_size);
        let gap = dp2px(scope.fm.config.all_item_gap);
        let width = size + gap;
        return items.map((iv, i, arr) => {
            let x = width * (i + 1);
            return { x: [x] }
        });
    }

    module.exports = VerticalMenu;
}

new function () {
    const { dp2px } = require("../modules/utils");
    const StyleMenu = require("./__style_menu__");
    const ItemView = require("../widgets/item_view");
    const FloatMenu = require("../index")

    /**
     * 扇形菜单样式
     * @param {FloatMenu} fm 
     */
    function HorizontalMenu(fm) {
        this.fm = fm;
        StyleMenu.call(this);
        init(this);
    }

    HorizontalMenu.prototype = {
        onUpdateMenuPosition() { //更新itemView坐标
            this.positions = createItemViewPosition(this);
        },
        onUpdateMenuWindow() {//更新Menu悬浮窗位置
            //更新悬浮窗大小
            let window = this.fm.menuWindow.window;
            if (!window) return null;
            let ivs = this.fm.viewUtil.item_views;
            let len = ivs.length;
            let size = dp2px(this.fm.config.all_item_size);
            let gap = dp2px(this.fm.config.all_item_gap);
            window.setSize((size + gap) * (len + 1), -2);
            //更新悬浮窗位置
            try {
                let x = this.fm.logoWindow.window.getX();
                let y = this.fm.logoWindow.window.getY();
                if (!this.fm.config.isLeftMargin)
                    x -= (size + gap) * (len + 1) - size;
                window.setPosition(x, y);
            } catch (error) {
                console.error('更新菜单悬浮窗位置失败', error);
            }
        },
        onUpdateMenuAnimation(f) {//菜单展开动画
            let e = Number(!this.fm.config.isLeftMargin);
            this.fm.viewUtil.item_views.forEach((iv, i) => {
                iv.setTranslationX(this.positions[i].x[e] * f);
            });
        }
    }

    HorizontalMenu.prototype = Object.assign(HorizontalMenu.prototype, StyleMenu.prototype);
    HorizontalMenu.prototype.constructor = HorizontalMenu;

    /** @param {HorizontalMenu} scope */
    function init(scope) {
        if (!scope.fm.menuWindow) return;
        scope.onUpdateMenuPosition();
        if (scope.fm.menuWindow.window) {
            scope.onUpdateMenuWindow(scope.fm.menuWindow.window);
        }
    }

    /**
     * 创建菜单ItemViews坐标
     * @param {HorizontalMenu} scope 
     */
    function createItemViewPosition(scope) {
        if (!scope.fm.viewUtil) return null;
        let items = scope.fm.viewUtil.item_views;
        let len = items.length;
        let size = dp2px(scope.fm.config.all_item_size);
        let gap = dp2px(scope.fm.config.all_item_gap);
        let w = size + gap;
        return items.map((iv, i, arr) => {
            return { x: [w * (i + 1), -(w * (len - i))] }
        });
    }

    module.exports = HorizontalMenu;
}


new function () {
    const { dp2px } = require("../modules/utils");
    const StyleMenu = require("./__style_menu__");
    const ItemView = require("../widgets/item_view");
    const FloatMenu = require("../index")

    /**
     * 扇形菜单样式
     * @param {FloatMenu} fm 
     */
    function CircularMenu(fm) {
        this.fm = fm;
        StyleMenu.call(this);
        init(this);
    }

    CircularMenu.prototype = {
        onUpdateMenuPosition() {//更新itemView坐标
            this.positions = createItemViewPosition(this);
        },
        onUpdateMenuWindow(window) {
            let window = this.fm.menuWindow.window;
            if (!window) return null;
            let size = dp2px(this.fm.config.all_item_size);
            let gap = dp2px(this.fm.config.all_item_gap);
            let padding = dp2px(this.fm.config.all_item_padding);
            //更新悬浮窗大小
            let [x, y] = [gap + size + padding, gap * 2 + size + padding]
            window.setSize(x, y);
            console.info("结果：", this.fm.logoWindow.window)
            //更新悬浮窗位置
            try {
                let wx = this.fm.logoWindow.window.getX();
                let wy = this.fm.logoWindow.window.getY();
                if (!this.fm.config.isLeftMargin) wx -= x;
                window.setPosition(wx + size / 2, wy - y / 2 + size / 2);
            } catch (error) {
                console.error('更新菜单悬浮窗位置失败', error);
                var a = context.resources.configuration.orientation;
                window.setPosition(5, ((a === 1 ? device.height : device.width) / 2) - 220);
            }
        },
        onUpdateMenuAnimation(f) {
            let e = Number(this.fm.config.isLeftMargin);
            this.fm.viewUtil.item_views.forEach((iv, i) => {
                iv.setTranslationX(this.positions.x[e][i] * f);
                iv.setTranslationY(this.positions.y[e][i] * f);
                iv.setScaleX(1 * f);
                iv.setScaleY(1 * f);
            });
        }
    }

    CircularMenu.prototype = Object.assign(CircularMenu.prototype, StyleMenu.prototype);
    CircularMenu.prototype.constructor = CircularMenu;

    /** @param {CircularMenu} scope */
    function init(scope) {
        if (!scope.fm.menuWindow) return;
        scope.onUpdateMenuPosition();
        if (scope.fm.menuWindow.window) {
            scope.onUpdateMenuWindow(scope.fm.menuWindow.window);
        }
    }

    /**
     * 创建菜单ItemViews坐标
     * @author I'm zz
     * @param {CircularMenu} scope 
     */
    function createItemViewPosition(scope) {
        let arr = { x: [], y: [] };
        let ivs = scope.fm.viewUtil.item_views;
        let gap = dp2px(scope.fm.config.all_item_gap);
        let len = ivs.length;
        if (!len) return null;
        let angle = scope.fm.config.angle / (len - 1);
        let firstAngle = 90 - scope.fm.config.angle / 2;
        let degree, value, x, y;
        for (let i = 0; i < 2; i++) {
            degree = i ? firstAngle : 360 - firstAngle;
            arr.x[i] = [];
            arr.y[i] = [];
            for (let e = 0; e < len; e++) {
                value = degree * Math.PI / 180;
                x = parseInt(gap * Math.sin(value));
                y = -parseInt(gap * Math.cos(value));
                arr.x[i][e] = (Math.abs(x) < 10 ? 0 : x);
                arr.y[i][e] = (Math.abs(y) < 10 ? 0 : y);
                i ? degree += angle : degree -= angle;
            }
        }
        return arr;
    }

    module.exports = CircularMenu;
}
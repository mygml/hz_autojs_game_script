

new function () {
    const ValueAnimator = android.animation.ValueAnimator
    const CycleInterpolator = android.view.animation.CycleInterpolator;
    const BounceInterpolator = android.view.animation.BounceInterpolator;
    const LinearInterpolator = android.view.animation.LinearInterpolator;
    const AccelerateInterpolator = android.view.animation.AccelerateInterpolator;
    const DecelerateInterpolator = android.view.animation.DecelerateInterpolator;
    const AccelerateDecelerateInterpolator = android.view.animation.AccelerateDecelerateInterpolator;

    const { defineValue } = require("../modules/utils");
    const FloatMenu = require("../index");

    const float_menu = Symbol('float_menu');
    const end = Symbol('callback');
    const start = Symbol('callback');
    const cancel = Symbol('callback');
    const repeat = Symbol('callback');
    const update = Symbol('callback');

    /**
     * 创建动画
     * @param {FloatMenu} fm FloatMenu
     * @param {number=} min 最小值 默认0;
     * @param {number=} max 最大值 默认1;
     * @param {number=} duration 动画执行时常 默认500;
     */
    function AnimationFloat(fm, min, max, duration) {
        this[float_menu] = fm;
        min = min || 0;
        max = max || 1;
        this.animator = new ValueAnimator.ofFloat(min, max);
        init(this);
        this.duration = duration || 500;
    }

    /** 动画插值器 */
    /** 动画从开始到结束，变化率是一个加速的过程。 */
    AnimationFloat.AccelerateInterpolator = AccelerateInterpolator;
    /** 动画从开始到结束，变化率是一个减速的过程。 */
    AnimationFloat.DecelerateInterpolator = DecelerateInterpolator;
    /** 动画从开始到结束，变化率是循环给定次数的正弦曲线。 */
    AnimationFloat.CycleInterpolator = CycleInterpolator;
    /** 动画从开始到结束，变化率是先加速后减速的过程。 */
    AnimationFloat.AccelerateDecelerateInterpolator = AccelerateDecelerateInterpolator;
    /** 动画从开始到结束，变化率是线性变化。 */
    AnimationFloat.LinearInterpolator = LinearInterpolator;
    AnimationFloat.BounceInterpolator = BounceInterpolator;

    AnimationFloat.prototype = {
        /**
         * 动画更新时 调用此方法
         * @param {animation_update} callback 
         */
        onAnimatoinUpdate(callback) {
            if ('function' === typeof callback)
                this[update] = callback;
            return this;
        },
        /**
         * 动画启动时 调用此方法
         * @param {animation_start} callback 
         */
        onAnimationStart(callback) {
            if ('function' === typeof callback)
                this[start] = callback;
            return this;
        },
        /**
         * 动画停止时 调用此方法
         * @param {animation_end} callback 
         */
        onAnimationEnd(callback) {
            if ('function' === typeof callback)
                this[end] = callback;
            return this;
        },
        /**
         * 动画取消时 调用此方法
         * @param {animation_cancel} callback 
         */
        onAnimationCancel(callback) {
            if ('function' === typeof callback)
                this[cancel] = callback;
            return this;
        },
        /**
         * 动画重复时 调用此方法
         * @param {animation_repeat} callback 
         */
        onAnimationRepeat(callback) {
            if ('function' === typeof callback)
                this[repeat] = callback;
            return this;
        },
        /**
         * 设置动画插值器
         * @param {*} interpolator 
         */
        setInterpolator(interpolator) {
            this.interpolator = interpolator;
            return this;
        },
        /**
         * 设置动画运行时常
         * @param {number} duration 
         */
        setDuration(duration) {
            this.duration = duration;
            return this;
        },
        /** 播放动画 */
        start() {
            if (this.animator.isRunning()) {
                this.animator.cancel();
            }
            this.animator.start();
        },
        /** 反向播放动画 */
        reverse() {
            if (this.animator.isRunning()) {
                this.animator.cancel();
            }
            this.animator.reverse();
        },

        defineValue
    }

    /**@type{AnimationFloat} */
    function init(scope) {
        scope.defineValue('duration', null, duration => {
            scope.animator.setDuration(duration);
        });
        scope.defineValue('interpolator', null, interpolator => {
            scope.animator.setInterpolator(interpolator);
        });

        scope[update] = new Function;
        scope[start] = new Function;
        scope[end] = new Function;
        scope[cancel] = new Function;
        scope[repeat] = new Function;
        scope.animator.addListener({
            onAnimationStart(animator, isReverse) {
                $ui.run(() => scope[start].apply(this, arguments));
                // scope[float_menu].emitter.emit('animation_start', animator, isReverse);
            },
            onAnimationEnd(animator, isReverse) {
                $ui.run(() => scope[end].apply(this, arguments));
                // scope[float_menu].emitter.emit('animation_end', animator, isReverse);
            },
            onAnimationCancel(animator) {
                $ui.run(() => scope[cancel].apply(this, arguments));
                // scope[float_menu].emitter.emit('animation_cancel', animator);
            },
            onAnimationRepeat(animator) {
                $ui.run(() => scope[repeat].apply(this, arguments));
                // scope[float_menu].emitter.emit('animation_repeat', animator);
            }
        });
        //2022/5/23修复不能在子线程中操作ui控件
        scope.animator.addUpdateListener({
            onAnimationUpdate(animator) {
                $ui.run(() => scope[update](animator));
            }
        });
    }

    //CallBack 动画回调方法
    /**
     * 动画更新回调方法
     * @callback animation_update
     * @param {animation} animator Animator
     */
    /**
     * 动画启动回调方法
     * @callback animation_start
     * @param {animation} animator Animator
     * @param {boolean} isReverse 动画是否反向播放
     */
    /**
     * 动画停止回调方法
     * @callback animation_end
     * @param {animation} animator Animator
     * @param {boolean} isReverse 动画是否反向播放
     */
    /**
     * 动画取消回调方法
     * @callback animation_cancel
     * @param {animation} animator Animator
     */
    /**
     * 动画反向播放回调方法
     * @callback animation_repeat
     * @param {animation} animator Animator
     */

    module.exports = AnimationFloat;
}
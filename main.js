"ui";
//ui.useAndroidResources()
ui.layout(
    <drawer id="drawer">
        <vertical>
            <appbar>
                <toolbar id="toolbar" title="华仔AutoJs游戏脚本脚手架" />
                <tabs id="tabs" />
            </appbar>
            <viewpager id="viewpager">
                <frame>
                    <vertical padding="15 10" bg="#eeeeee">
                        <ScrollView h="auto" layout_weight="25">
                        <vertical h="auto" layout_weight="25">
                            <card contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
                                <vertical id="deiveceBaseInfo" visibility="visible">
                                    <text text="基本信息:" textSize="22sp" textColor="#210303" marginBottom="5px" />
                                    <horizontal h="80px">
                                        <text text="屏幕宽度:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                                        <text id="屏幕宽度" text="" textColor="#210303" textSize="16sp" h="*" w="*" gravity="left|center" layout_weight="2" />
                                    </horizontal>
                                    <horizontal h="80px">
                                        <text text="屏幕高度:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                                        <text id="屏幕高度" text="" textColor="#210303" textSize="16sp" h="*" w="*" gravity="left|center" layout_weight="2" />
                                    </horizontal>
                                    <horizontal h="80px">
                                        <text text="DPI:" textColor="#210303" textSize="16sp" h="*" w="400px" gravity="left|center" layout_weight="1" />
                                        <text id="DPI" text="" textColor="#210303" textSize="16sp" h="*" w="*" gravity="left|center" layout_weight="2" />
                                    </horizontal>
                                </vertical>
                            </card>
                        </vertical>
                        </ScrollView> 
                        <horizontal layout_weight="1" gravity="center" w="*" marginTop="30px">
                            <button id="help" layout_gravity="center"  text="使用介绍" w="300px" style="Widget.AppCompat.Button.Colored" bg="#827f7f" />
                            <button id="gitee" layout_gravity="center"  text="开源地址" w="300px" marginLeft="50px" style="Widget.AppCompat.Button.Colored" bg="#ff5723" />
                        </horizontal>
                    </vertical>
                </frame>
                <frame>
                    <vertical padding="15 10" bg="#eeeeee">
                        <card contentPadding="50px 20px 50px 20px" cardBackgroundColor="#ffffff" cardCornerRadius="15px" cardElevation="15px">
                            <vertical margin="5 2" layout_gravity="center" bg="#ffffff">
                                <Switch h="30" text="无障碍服务" id="autoService" checked="true" />
                                <Switch h="30" text="悬浮窗" id="floatyPermission" checked="true" />
                                <Switch h="30" text="前台服务" id="foregroundService" checked="true" />
                                <Switch h="30" text="无障碍稳定模式" id="stableMode" checked="true" />
                                <Switch h="30" text="截图权限" id="screenCapturePermission" checked="true" />
                                <Switch h="30" text="后台弹出权限" id="backgroundOpenPermission" checked="true" />
                            </vertical>
                        </card>
                    </vertical>
                </frame>
                <frame>
                    <vertical padding="15 10" bg="#eeeeee">
                        <ScrollView h="auto" layout_weight="25">
                            <vertical id="uiView">
                            </vertical>
                        </ScrollView>
                        <horizontal layout_weight="1" gravity="center" marginTop="30px">
                            <button id="loadSetting" layout_gravity="center" text="载入配置" style="Widget.AppCompat.Button.Colored" bg="#827f7f" />
                            <button id="saveSetting" layout_gravity="center" text="保存配置" style="Widget.AppCompat.Button.Colored" bg="#ff5723" marginLeft="100px" marginRight="100px" />
                            <button id="startScript" layout_gravity="center" text="启动脚本" style="Widget.AppCompat.Button.Colored" bg="#04a9f5" />
                        </horizontal>
                    </vertical>
                </frame>
                <frame>
                    <button id="clearLog" text="清空日志" gravity="center" layout_gravity="right" padding="0" h="40" w="80" />
                    <globalconsole id="globalconsole" w="*" h="*" />
                </frame>
            </viewpager>
        </vertical>
    </drawer>
);


function getCurrentTime() {
    var date = new Date();//当前时间
    var month = zeroFill(date.getMonth() + 1);//月
    var day = zeroFill(date.getDate());//日
    var hour = zeroFill(date.getHours());//时
    //当前时间
    var curTime = date.getFullYear() + month + day
        + hour;
    return curTime;
}

function zeroFill(i) {
    if (i >= 0 && i <= 9) {
        return "0" + i;
    } else {
        return String(i);
    }
}


 //设置滑动页面的标题
 ui.viewpager.setTitles(["设备信息", "权限设置", "功能设置", "运行日志"]);
 //让滑动页面和标签栏联动
 ui.tabs.setupWithViewPager(ui.viewpager);
activity.setSupportActionBar(ui.toolbar)

let permission = require("./module/permission.js")
permission.init()

// 获取当前时间字符串
let currenTimes = getCurrentTime()
console.setGlobalLogConfig({
    file: "/sdcard/autoJsLog/log" + currenTimes + ".txt"
})


threads.start(() => {
    try {
        while (true) {
            sleep(1000)
            let tempTimes = getCurrentTime()
            if (currenTimes !== tempTimes) {
                currenTimes = tempTimes
                console.setGlobalLogConfig({
                    file: "/sdcard/autoJsLog/log" + currenTimes + ".txt"
                })
            }
        }
    } catch (error) {
        console.error("设置日志错误", error)
    }
})

// 自定义日志颜色
ui.globalconsole.setColor("V", "#bdbdbd");
ui.globalconsole.setColor("D", "#795548");
ui.globalconsole.setColor("I", "#1de9b6");
ui.globalconsole.setColor("W", "#673ab7");
ui.globalconsole.setColor("E", "#b71c1c");

ui.clearLog.on("click", () => {
    ui.globalconsole.clear()
})


ui.help.on("click", () => {
    app.openUrl("https://www.zjh336.cn/?id=2117")
})
ui.gitee.on("click",()=>{
    app.openUrl("https://gitee.com/zjh336/hz_autojs_game_script")
})

let isCanFinish = false;
ui.emitter.on("back_pressed", (e) => {
    if (!isCanFinish) {
        toast("再按一次返回键退出");
        isCanFinish = true;
        isCanFinishTimeout = setTimeout(() => {
            isCanFinish = false;
        }, 700);
        e.consumed = true;
    } else {
        clearTimeout(isCanFinishTimeout);
        e.consumed = false;
    }
});

let FloatMenu = require('./@se7en/float_menu-rhino');
let runScript = require("./module/runScript")
runScript.init(FloatMenu)
